core = 7.x
api = 2

; Drupal Core
projects[drupal][version] = 7.32
projects[drupal][patch][2033883] = http://drupal.org/files/orderedResults.patch

; GK Profile
projects[gk][type] = profile
projects[gk][download][type] = git
projects[gk][download][url] = git@bitbucket.org:greeneking/gk-profile.git
projects[gk][download][tag] = 7.x-3.24
